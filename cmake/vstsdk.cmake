include(FetchContent)

FetchContent_Declare(
  vstsdk
  GIT_REPOSITORY https://github.com/steinbergmedia/vst3sdk.git
  GIT_TAG        v3.7.1_build_50
  GIT_SHALLOW    ON
  GIT_SUBMODULES base cmake pluginterfaces public.sdk vstgui4
)

FetchContent_GetProperties(vstsdk)
if(NOT vstsdk_POPULATED)
  FetchContent_Populate(vstsdk)

  set(SMTG_RUN_VST_VALIDATOR OFF CACHE BOOL "Run VST validator on VST3 plug-ins" FORCE)
  set(SMTG_ADD_VST3_PLUGINS_SAMPLES OFF CACHE BOOL "" FORCE)
  set(SMTG_ADD_VST3_HOSTING_SAMPLES OFF CACHE BOOL "" FORCE)
  set(SMTG_CREATE_VST3_LINK OFF CACHE BOOL "")
  set(VSTGUI_STANDALONE OFF)
  set(VSTGUI_TOOLS OFF)

  # NOTE: Workaround for adding vstsdk as a subdirectory.
  # We need to include SMTG_Global.cmake before add_subdirectory otherwise some
  # variables will only be set in the current scope and not in ${vstsdk_SOURCE_DIR}
  list(APPEND CMAKE_MODULE_PATH "${vstsdk_SOURCE_DIR}/cmake/modules")
  include(SMTG_Global)

  add_subdirectory(${vstsdk_SOURCE_DIR} ${vstsdk_BINARY_DIR} EXCLUDE_FROM_ALL)

  # suppress all warnings from vstsdk and vstgui
  if(UNIX)
    if(XCODE)
      set_target_properties(sdk base validator vstgui vstgui_support vstgui_uidescription PROPERTIES
      XCODE_ATTRIBUTE_GCC_WARN_ABOUT_DEPRECATED_FUNCTIONS NO
      XCODE_ATTRIBUTE_CLANG_ENABLE_OBJC_WEAK $<$<CONFIG:Debug>:"YES">
      XCODE_ATTRIBUTE_GCC_WARN_INHIBIT_ALL_WARNINGS "YES")
    else()
      set_target_properties(sdk base validator vstgui vstgui_support vstgui_uidescription PROPERTIES COMPILE_FLAGS "-w")
    endif()
  elseif(MSVC)
    set_target_properties(sdk base validator vstgui vstgui_support vstgui_uidescription PROPERTIES COMPILE_FLAGS "/w")
  endif()

  # NOTE: Workaround for Linux, missing include file in threadchecker_linux.cpp
  if (CMAKE_SYSTEM_NAME MATCHES "Linux")
    target_compile_options(sdk PUBLIC --include=cstdio)
  endif()
endif()

set(SDK_ROOT ${vstsdk_SOURCE_DIR})
set(public_sdk_SOURCE_DIR ${SDK_ROOT}/public.sdk)

# Include as SYSTEM to avoid warnings from vstsdk and vstgui.
include_directories(SYSTEM ${SDK_ROOT})
include_directories(SYSTEM ${SDK_ROOT}/vstgui4)
