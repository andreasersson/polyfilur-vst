find_program(CPPCHECK_BIN NAMES cppcheck)

if(CPPCHECK_BIN)
  set(CPPCHECK_ADDITIONAL_CHECKS warning style performance portability CACHE STRING "Enable additional cppcheck checks")
  set(CPPCHECK_TEMPLATE "[{severity}] CWE{cwe} {id} {message}\\n{file}:{line}\\n{code}" CACHE STRING "Format cppcheck error messages")
  option(CPPCHECK_QUIET "Do not show cppcheck progress reports." ON)

  # generate compile_commands.json to use with cppcheck
  set(CMAKE_EXPORT_COMPILE_COMMANDS ON)
endif()

set(target cppcheck)

if(CPPCHECK_BIN AND NOT TARGET ${target})
  string(REPLACE ";" "," additional_checks "${CPPCHECK_ADDITIONAL_CHECKS}")
  if(${CPPCHECK_QUIET})
    set(quiet --quiet)
  endif()
  add_custom_target(${target}
                    COMMAND ${CPPCHECK_BIN}
                    ${quiet}
                    --enable=${additional_checks}
                    --template=${CPPCHECK_TEMPLATE}
                    --project=${CMAKE_BINARY_DIR}/compile_commands.json
                    --suppressions-list=${CMAKE_CURRENT_SOURCE_DIR}/cppcheck_suppressions.txt
                    VERBATIM)
endif()
