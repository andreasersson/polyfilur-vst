/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of polyfilur.
 *
 * polyfilur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * polyfilur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with polyfilur.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef POLYFILUR_CONTROLLER_H
#define POLYFILUR_CONTROLLER_H

#include <filurvst/gui/aboutbox_controller.h>

#include <public.sdk/source/vst/vsteditcontroller.h>
#include <pluginterfaces/base/smartpointer.h>
#include <vstgui/plugin-bindings/vst3editor.h>

namespace polyfilur {

class Controller : public Steinberg::Vst::EditController,
    public Steinberg::Vst::IMidiMapping,
    public  VSTGUI::VST3EditorDelegate {
 public:
  Controller();

  Steinberg::tresult initialize(FUnknown* context) SMTG_OVERRIDE;
  Steinberg::tresult terminate() SMTG_OVERRIDE;
  Steinberg::tresult setComponentState(Steinberg::IBStream* state) SMTG_OVERRIDE;

  Steinberg::tresult getMidiControllerAssignment(Steinberg::int32 bus_index,
                                                 Steinberg::int16 channel,
                                                 Steinberg::Vst::CtrlNumber midi_cc,
                                                 Steinberg::Vst::ParamID& id) SMTG_OVERRIDE;

  virtual Steinberg::tresult PLUGIN_API openAboutBox(Steinberg::TBool onlyCheck) SMTG_OVERRIDE;

  Steinberg::IPlugView* createView(Steinberg::FIDString name) SMTG_OVERRIDE;

  virtual VSTGUI::CView* createCustomView(VSTGUI::UTF8StringPtr name,
                                          const VSTGUI::UIAttributes& attributes,
                                          const VSTGUI::IUIDescription* description,
                                          VSTGUI::VST3Editor* editor) override;
  virtual VSTGUI::COptionMenu* createContextMenu(const VSTGUI::CPoint& pos,
                                                 VSTGUI::VST3Editor* editor) override;

  static Steinberg::FUID cid;

  OBJ_METHODS (Controller, EditController)DEFINE_INTERFACES
  DEF_INTERFACE (IMidiMapping)
  END_DEFINE_INTERFACES (EditController)
  REFCOUNT_METHODS(EditController)

 private:
  Steinberg::IPtr<filurvst::gui::AboutBoxController> m_aboutbox;
};

}  // namespace polyfilur

#endif // POLYFILUR_CONTROLLER_H
