/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of polyfilur.
 *
 * polyfilur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * polyfilur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with polyfilur.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "parameter_state.h"

#include "parameter_ids.h"

#include <filur/polyfilur/pf_parameters.h>
#include <filur/tempo_sync.h>

namespace polyfilur {

static constexpr double osc_min_waveform = PF_OSC_WAVEFORM_SINE;
static constexpr double osc_max_waveform = PF_OSC_WAVEFORM_PULSE;
static constexpr int osc_min_coarse = -2400;
static constexpr int osc_max_coarse = 2400;
static constexpr int osc_min_fine_tune = -50;
static constexpr int osc_max_fine_tune = 50;
static constexpr int osc_min_vibrato_depth = 0;
static constexpr int osc_max_vibrato_depth = 100;

static constexpr double filter_min_cutoff = 0;
static constexpr double filter_max_cutoff = 13000;
static constexpr double filter_min_resonance = 0.5;
static constexpr double filter_max_resonance = 10;
static constexpr double filter_min_env_depth = -4800;
static constexpr double filter_max_env_depth = 4800;
static constexpr double filter_min_lfo_depth = -2400;
static constexpr double filter_max_lfo_depth = 2400;
static constexpr double filter_min_velocity_depth = -2400;
static constexpr double filter_max_velocity_depth = 2400;
static constexpr double filter_min_modwheel_depth = -4800;
static constexpr double filter_max_modwheel_depth = 4800;
static constexpr double filter_keyboard_follow_octaves = 4.0;

static constexpr double adsr_min_attack = 1e-3;
static constexpr double adsr_max_attack = 1000e-3;
static constexpr double adsr_min_decay = 10e-3;
static constexpr double adsr_max_decay = 4000e-3;
static constexpr double adsr_min_sustain = 0.0;
static constexpr double adsr_max_sustain = 1.0;
static constexpr double adsr_min_release = 10e-3;
static constexpr double adsr_max_release = 4000e-3;

static constexpr double lfo_min_waveform = PF_LFO_WAVEFORM_SINE;
static constexpr double lfo_max_waveform = PF_LFO_WAVEFORM_SAW;
static constexpr double lfo_min_speed = 0.001;
static constexpr double lfo_max_speed = 10.0;
static constexpr double lfo_min_phase = -1.0;
static constexpr double lfo_max_phase = 1.0;
static constexpr double lfo_min_tempo_sync = TEMPO_SYNC_FREE_RUNNING;
static constexpr double lfo_max_tempo_sync = TEMPO_SYNC_NUM_TEMPO_SYNCS - 1;
static constexpr double lfo_min_restart = PF_LFO_RESTART_FALSE;
static constexpr double lfo_max_restart = PF_LFO_RESTART_TRUE;

static constexpr double min_pitch_bend_range = 0.0;
static constexpr double max_pitch_bend_range = 24.0;

static constexpr double portamento_min_time = 1e-3;
static constexpr double portamento_max_time = 200e-3;

static constexpr double min_velocity_depth = 0.0;
static constexpr double max_velocity_depth = 1.0;

static constexpr double min_velocity_curve = -1.0;
static constexpr double max_velocity_curve = 1.0;

static constexpr double min_voice_mode = 0.0;
static constexpr double max_voice_mode = 2.0;

static constexpr double min_volume = -40.0;
static constexpr double max_volume = 0.0;

ParameterState::ParameterState() {

  keboard_follow_factor = filter_keyboard_follow_octaves / (filter_max_cutoff / 1200);

  add(kMasterVolume, -9.0, min_volume, max_volume);

  addUInt(kOsc1Waveform, PF_OSC_WAVEFORM_PULSE, osc_min_waveform, osc_max_waveform);
  addInt(kOsc1Coarse, 0, osc_min_coarse, osc_max_coarse);
  add(kOsc1FineTune, 0.0, osc_min_fine_tune, osc_max_fine_tune);
  add(kOsc1VibratoDepth, 0.0, osc_min_vibrato_depth, osc_max_vibrato_depth);
  add(kOsc1PulseWidth, 0.5);
  add(kOsc1PwmDepth, 0.0);

  addUInt(kOsc2Waveform, PF_OSC_WAVEFORM_PULSE, osc_min_waveform, osc_max_waveform);
  addInt(kOsc2Coarse, 0, osc_min_coarse, osc_max_coarse);
  add(kOsc2FineTune, 0.0, osc_min_fine_tune, osc_max_fine_tune);
  add(kOsc2VibratoDepth, 0.0, osc_min_vibrato_depth, osc_max_vibrato_depth);
  add(kOsc2PulseWidth, 0.5);
  add(kOsc2PwmDepth, 0.0);

  add(kMixer1Ch1Volume, -6.0, min_volume, max_volume);
  addUInt(kMixer1Ch1Enabled, PF_MIXER_CH_ON, PF_MIXER_CH_OFF, PF_MIXER_CH_BYPASS);
  add(kMixer1Ch2Volume, -6.0, min_volume, max_volume);
  addUInt(kMixer1Ch2Enabled, PF_MIXER_CH_OFF, PF_MIXER_CH_OFF, PF_MIXER_CH_BYPASS);
  add(kMixer1Ch3Volume, -6.0, min_volume, max_volume);
  addUInt(kMixer1Ch3Enabled, PF_MIXER_CH_OFF, PF_MIXER_CH_OFF, PF_MIXER_CH_BYPASS);

  addUInt(kFilter1Type, PF_FILTER_TYPE_LOWPASS, PF_FILTER_TYPE_LOWPASS, PF_FILTER_TYPE_BANDPASS);
  addUInt(kFilter1KeyboardFollow, PF_KBF_DISABLED, PF_KBF_DISABLED, PF_KBF_ENABLED);
  add(kFilter1Cutoff, 9600, filter_min_cutoff, filter_max_cutoff);
  add(kFilter1Resonance, 1.0, filter_min_resonance, filter_max_resonance);
  add(kFilter1EnvDepth, 0.0, filter_min_env_depth, filter_max_env_depth);
  add(kFilter1LfoDepth, 0.0, filter_min_lfo_depth, filter_max_lfo_depth);
  add(kFilter1VelocityDepth, 0.0, filter_min_velocity_depth, filter_max_velocity_depth);
  add(kFilter1ModWheelDepth, 0.0, filter_min_modwheel_depth, filter_max_modwheel_depth);

  addSqr(kAdsr1Attack, 1e-3, adsr_min_attack, adsr_max_attack);
  addSqr(kAdsr1Decay, 100e-3, adsr_min_decay, adsr_max_decay);
  addSqr(kAdsr1Sustain, 1.0, adsr_min_sustain, adsr_max_sustain);
  addSqr(kAdsr1Release, 50e-3, adsr_min_release, adsr_max_release);

  addSqr(kAdsr2Attack, 1e-3, adsr_min_attack, adsr_max_attack);
  addSqr(kAdsr2Decay, 500e-3, adsr_min_decay, adsr_max_decay);
  addSqr(kAdsr2Sustain, 0.0, adsr_min_sustain, adsr_max_sustain);
  addSqr(kAdsr2Release, 50e-3, adsr_min_release, adsr_max_release);

  addUInt(kOscLfoWaveform, PF_LFO_WAVEFORM_TRIANGLE, lfo_min_waveform, lfo_max_waveform);
  addSqr(kOscLfoSpeed, 0.25, lfo_min_speed, lfo_max_speed);
  addUInt(kOscLfoTempoSync, 0, lfo_min_tempo_sync, lfo_max_tempo_sync);

  addUInt(kFilterLfoWaveform, PF_LFO_WAVEFORM_SINE, lfo_min_waveform, lfo_max_waveform);
  addSqr(kFilterLfoSpeed, 0.25, lfo_min_speed, lfo_max_speed);
  add(kFilterLfoPhase, 0.0, lfo_min_phase, lfo_max_phase);
  addUInt(kFilterLfoTempoSync, 0, lfo_min_tempo_sync, lfo_max_tempo_sync);
  addUInt(kFilterLfoRestart, PF_LFO_RESTART_FALSE, lfo_min_restart, lfo_max_restart);

  add(kVelocityDepth, 0.0, min_velocity_depth, max_velocity_depth);
  add(kVelocityCurve, 0.0, min_velocity_curve, max_velocity_curve);

  addUInt(kVoiceMode, 0, min_voice_mode, max_voice_mode);

  addUInt(kPortamento, 0.0, 0.0, 1.0);
  addSqr(kPortamentoTime, 50e-3, portamento_min_time, portamento_max_time);

  addUInt(kPitchBendRange, 2.0, min_pitch_bend_range, max_pitch_bend_range);

  add(kPitchBend, 0.5, 0.0, 1.0);
  add(kModWheel, 0.0);
  m_parameter_info.at(kPitchBend).runtime = true;
  m_parameter_info.at(kModWheel).runtime = true;
}

}  // namespace polyfilur

