/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of polyfilur.
 *
 * polyfilur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * polyfilur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with polyfilur.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef POLYFILUR_PARAMETER_ID_H
#define POLYFILUR_PARAMETER_ID_H

namespace polyfilur {

typedef enum {
  kOsc1Waveform = 0,
  kOsc1PulseWidth,
  kOsc1PwmDepth,
  kOsc1Coarse,
  kOsc1FineTune,
  kOsc1VibratoDepth,

  kOsc2Waveform = 10,
  kOsc2PulseWidth,
  kOsc2PwmDepth,
  kOsc2Coarse,
  kOsc2FineTune,
  kOsc2VibratoDepth,

  kFilter1Type = 20,
  kFilter1KeyboardFollow,
  kFilter1Cutoff,
  kFilter1Resonance,
  kFilter1EnvDepth,
  kFilter1LfoDepth,
  kFilter1VelocityDepth,
  kFilter1ModWheelDepth,

  kMixer1Ch1Volume = 30,
  kMixer1Ch1Enabled,
  kMixer1Ch2Volume,
  kMixer1Ch2Enabled,
  kMixer1Ch3Volume,
  kMixer1Ch3Enabled,

  kAdsr1Attack = 40,
  kAdsr1Decay,
  kAdsr1Sustain,
  kAdsr1Release,

  kAdsr2Attack = 50,
  kAdsr2Decay,
  kAdsr2Sustain,
  kAdsr2Release,

  kOscLfoWaveform = 60,
  kOscLfoSpeed,
  kOscLfoTempoSync,

  kFilterLfoWaveform = 70,
  kFilterLfoSpeed,
  kFilterLfoTempoSync,
  kFilterLfoRestart,
  kFilterLfoPhase,

  kPortamento = 80,
  kPortamentoTime,

  kMasterVolume = 90,
  kVelocityDepth,
  kVelocityCurve,

  kVoiceMode = 100,
  kPitchBendRange,

  kPitchBend = 200,
  kModWheel
} parameter_id_t;

}  // namespace polyfilur

#endif // POLYFILUR_PARAMETER_ID_H
