/*
 * Copyright 2018-2020 Andreas Ersson
 *
 * This file is part of polyfilur.
 *
 * polyfilur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * polyfilur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with polyfilur.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "processor.h"

#include "parameter_ids.h"

#include <pluginterfaces/vst/ivstevents.h>
#include <pluginterfaces/vst/ivstparameterchanges.h>

#include <algorithm>
#include <cassert>

namespace polyfilur {

Steinberg::FUID Processor::cid(0x199AA3BD, 0x5A7E436F, 0xAF88A516, 0xBF12D946);


static pf_osc_waveform_t value_to_osc_waveform(Steinberg::Vst::ParamValue value) {
  int int_value = static_cast<int>(value);
  pf_osc_waveform_t ret_val = PF_OSC_WAVEFORM_SINE;

  switch (int_value) {
    case PF_OSC_WAVEFORM_SINE:
      ret_val = PF_OSC_WAVEFORM_SINE;
      break;

    case PF_OSC_WAVEFORM_SAW:
      ret_val = PF_OSC_WAVEFORM_SAW;
      break;

    case PF_OSC_WAVEFORM_PULSE:
      ret_val = PF_OSC_WAVEFORM_PULSE;
      break;

    default:
      assert(false);
      break;
  }

  return ret_val;
}

static pf_filter_type_t value_to_filter_type(Steinberg::Vst::ParamValue value) {
  int int_value = static_cast<int>(value);
  pf_filter_type_t ret_val = PF_FILTER_TYPE_LOWPASS;
  switch (int_value) {
    case PF_FILTER_TYPE_LOWPASS:
      ret_val = PF_FILTER_TYPE_LOWPASS;
      break;

    case PF_FILTER_TYPE_HIGHPASS:
      ret_val = PF_FILTER_TYPE_HIGHPASS;
      break;

    case PF_FILTER_TYPE_BANDPASS:
      ret_val = PF_FILTER_TYPE_BANDPASS;
      break;

    default:
      assert(false);
      break;
  }

  return ret_val;
}

static pf_kbf_enabled_t value_to_kbf_enabled(Steinberg::Vst::ParamValue value) {
  pf_kbf_enabled_t ret_val = PF_KBF_DISABLED;
  if (value >= 0.5) {
    ret_val = PF_KBF_ENABLED;
  }

  return ret_val;
}

static pf_mixer_ch_state_t value_to_mixer_ch_state(Steinberg::Vst::ParamValue value) {
  int int_value = static_cast<int>(value);
  pf_mixer_ch_state_t ret_val = PF_MIXER_CH_OFF;
  switch (int_value) {
    case PF_MIXER_CH_OFF:
      ret_val = PF_MIXER_CH_OFF;
      break;

    case PF_MIXER_CH_ON:
      ret_val = PF_MIXER_CH_ON;
      break;

    case PF_MIXER_CH_BYPASS:
      ret_val = PF_MIXER_CH_BYPASS;
      break;

    default:
      assert(false);
      break;
  }

  return ret_val;
}

static pf_lfo_waveform_t value_to_lfo_waveform(Steinberg::Vst::ParamValue value) {
  int int_value = static_cast<int>(value);
  pf_lfo_waveform_t ret_val = PF_LFO_WAVEFORM_SINE;
  switch (int_value) {
    case PF_LFO_WAVEFORM_SINE:
      ret_val = PF_LFO_WAVEFORM_SINE;
      break;

    case PF_LFO_WAVEFORM_TRIANGLE:
      ret_val = PF_LFO_WAVEFORM_TRIANGLE;
      break;

    case PF_LFO_WAVEFORM_SAW:
      ret_val = PF_LFO_WAVEFORM_SAW;
      break;

    default:
      assert(false);
      break;
  }

  return ret_val;
}

static pf_lfo_restart_t value_to_lfo_restart(Steinberg::Vst::ParamValue value) {
  pf_lfo_restart_t ret_val = PF_LFO_RESTART_FALSE;

  if (value >= 0.5) {
    ret_val = PF_LFO_RESTART_TRUE;
  }

  return ret_val;
}

static pf_portamento_enabled_t value_to_portamento_enabled(Steinberg::Vst::ParamValue value) {
  pf_portamento_enabled_t ret_val = PF_PORTAMENTO_DISABLED;

  if (value >= 0.5) {
    ret_val = PF_PORTAMENTO_ENABLED;
  }

  return ret_val;
}

static pf_voice_mode_t value_to_voice_mode(Steinberg::Vst::ParamValue value) {
  int int_value = static_cast<int>(value);
  pf_voice_mode_t ret_val = PF_VOICE_MODE_POLY;
  switch (int_value) {
    case PF_VOICE_MODE_POLY:
      ret_val = PF_VOICE_MODE_POLY;
      break;

    case PF_VOICE_MODE_MONO:
      ret_val = PF_VOICE_MODE_MONO;
      break;

    case PF_VOICE_MODE_MONOLEGATO:
      ret_val = PF_VOICE_MODE_MONOLEGATO;
      break;

    default:
      assert(false);
      break;
  }

  return ret_val;
}

static void handle_parameter_changes(Steinberg::Vst::IParameterChanges* parameter_changes,
                              ParameterState& state,
                              pf_parameters_t& parameters);
static void set_parameter(Steinberg::Vst::ParamID parameter_id, double value, pf_parameters_t &parameters);
static void set_state_parameters(const ParameterState& state, pf_parameters_t &parameters, polyfilur_t& polyfilur);
static void update_tempo(Steinberg::Vst::ProcessContext* process_context, pf_parameters_t &parameters);
static void to_sample32(const double* left_buffer, const double* right_buffer, const Steinberg::Vst::ProcessData& data);
static void to_sample64(const double* left_buffer, const double* right_buffer, const Steinberg::Vst::ProcessData& data);

Processor::Processor(const Steinberg::FUID& controller_id) {
  setControllerClass(controller_id);
  processContextRequirements.needTempo();
  setDefaultParameterState();
}

Steinberg::tresult Processor::initialize(FUnknown* context) {
  Steinberg::tresult ret_val = AudioEffect::initialize(context);
  if (Steinberg::kResultTrue == ret_val) {
    addAudioOutput(STR16("output"), Steinberg::Vst::SpeakerArr::kStereo);
    addEventInput(STR16("event input"), 1);
  }

  return ret_val;
}

Steinberg::tresult Processor::setState(Steinberg::IBStream* state) {
  Steinberg::tresult ret_val = m_state.setState(state);

  if (m_is_active) {
    set_state_parameters(m_state, m_parameters, m_polyfilur);
  }

  return ret_val;
}

Steinberg::tresult Processor::getState(Steinberg::IBStream* state) {
  Steinberg::tresult ret_val = m_state.getState(state);

  return ret_val;
}

Steinberg::tresult Processor::setBusArrangements(Steinberg::Vst::SpeakerArrangement* inputs,
                                                 Steinberg::int32 num_inputs,
                                                 Steinberg::Vst::SpeakerArrangement* outputs,
                                                 Steinberg::int32 num_outputs) {
  Steinberg::tresult ret_val = Steinberg::kResultFalse;

  if ((0 == num_inputs) && (1 == num_outputs)
      && (Steinberg::Vst::SpeakerArr::kStereo == outputs[0])) {
    ret_val = AudioEffect::setBusArrangements(inputs, num_inputs, outputs, num_outputs);
  }

  return ret_val;
}

Steinberg::tresult Processor::canProcessSampleSize(Steinberg::int32 symbolic_sample_size) {
  Steinberg::tresult ret_val = Steinberg::kResultFalse;

  if ((Steinberg::Vst::kSample32 == symbolic_sample_size)
      || (Steinberg::Vst::kSample64 == symbolic_sample_size)) {
    ret_val = Steinberg::kResultTrue;
  }

  return ret_val;
}

Steinberg::tresult Processor::setActive(Steinberg::TBool state) {
  m_is_active = state;
  if (state) {
    pf_parameters_init(processSetup.sampleRate, m_state.keboard_follow_factor, &m_parameters);
    polyfilur_init(processSetup.sampleRate, &m_polyfilur);

    m_left_buffer.resize(processSetup.maxSamplesPerBlock);
    m_right_buffer.resize(processSetup.maxSamplesPerBlock);

    set_state_parameters(m_state, m_parameters, m_polyfilur);
  }

  return AudioEffect::setActive(state);
}

Steinberg::tresult Processor::process(Steinberg::Vst::ProcessData& data) {
  Steinberg::tresult result = Steinberg::kResultTrue;

  handle_parameter_changes(data.inputParameterChanges, m_state, m_parameters);

  if ((data.numOutputs < 1) || (data.outputs[0].numChannels < 2)) {
    return Steinberg::kResultTrue;
  }

  Steinberg::int32 number_of_events = 0;
  Steinberg::Vst::IEventList* input_events = data.inputEvents;
  if (nullptr != input_events) {
    number_of_events = input_events->getEventCount();
  }

  update_tempo(data.processContext, m_parameters);

  assert(static_cast<size_t>(data.numSamples) <= m_left_buffer.size());
  assert(static_cast<size_t>(data.numSamples) <= m_right_buffer.size());
  double* out_left = m_left_buffer.data();
  double* out_right = m_right_buffer.data();
  memset(out_left, 0, sizeof(double) * m_left_buffer.size());
  memset(out_right, 0, sizeof(double) * m_left_buffer.size());

  size_t active_voices = polyfilur_num_active_voices(&m_polyfilur);

  Steinberg::int32 number_of_frames = data.numSamples;
  Steinberg::int32 frame_offset = 0;

  Steinberg::Vst::Event event;

  for (Steinberg::int32 i = 0; i < number_of_events; i++) {
    Steinberg::tresult handle_event = input_events->getEvent(i, event);

    if (Steinberg::kResultTrue == handle_event) {
      switch (event.type) {
        // supported event types
        case Steinberg::Vst::Event::kNoteOnEvent:
        case Steinberg::Vst::Event::kNoteOffEvent:
          break;

        // ignore all other event types
        default:
          handle_event = Steinberg::kResultFalse;
          break;
      }
    }

    if (Steinberg::kResultTrue == handle_event) {
      Steinberg::int32 number_of_sub_frames = event.sampleOffset - frame_offset;
      frame_offset = event.sampleOffset;
      number_of_sub_frames = std::min(number_of_sub_frames, number_of_frames);

      if (number_of_sub_frames > 0) {
        polyfilur_process(out_left, out_right, number_of_sub_frames, &m_parameters, &m_polyfilur);
        out_left += number_of_sub_frames;
        out_right += number_of_sub_frames;
        number_of_frames -= number_of_sub_frames;
        active_voices += polyfilur_num_active_voices(&m_polyfilur);
      }

      switch (event.type) {
        case Steinberg::Vst::Event::kNoteOnEvent:
          polyfilur_note_on(event.noteOn.pitch, event.noteOn.velocity, &m_parameters, &m_polyfilur);
          break;

        case Steinberg::Vst::Event::kNoteOffEvent:
          polyfilur_note_off(event.noteOn.pitch, &m_parameters, &m_polyfilur);
          break;

        default:
          break;
      }
    }
  }

  if (number_of_frames > 0) {
    polyfilur_process(out_left, out_right, number_of_frames, &m_parameters, &m_polyfilur);
    active_voices += polyfilur_num_active_voices(&m_polyfilur);

  }

  if (Steinberg::Vst::kSample64 == data.symbolicSampleSize) {
    to_sample64(m_left_buffer.data(), m_right_buffer.data(), data);
  } else {
    to_sample32(m_left_buffer.data(), m_right_buffer.data(), data);
  }

  if (0 == active_voices) {
    data.outputs[0].silenceFlags = 0x03;
  }

  return result;
}

static void handle_parameter_changes(Steinberg::Vst::IParameterChanges* parameter_changes,
                              ParameterState& state,
                              pf_parameters_t& parameters) {
  Steinberg::int32 parameter_count = 0;
  if (nullptr != parameter_changes) {
    parameter_count = parameter_changes->getParameterCount();
  }

  for (Steinberg::int32 i = 0; i < parameter_count; i++) {
    Steinberg::tresult result = Steinberg::kResultTrue;
    Steinberg::Vst::IParamValueQueue* queue = parameter_changes->getParameterData(i);
    if (nullptr == queue) {
      result = Steinberg::kResultFalse;
    }

    if ((Steinberg::kResultTrue == result) && (0 == queue->getPointCount())) {
      result = Steinberg::kResultFalse;
    }

    Steinberg::Vst::ParamValue value = 0;
    Steinberg::int32 sample_offset = 0;
    if (Steinberg::kResultTrue == result) {
      result = queue->getPoint(queue->getPointCount() - 1, sample_offset, value);
    }

    if (Steinberg::kResultTrue == result) {
      state.setNormalized(queue->getParameterId(), value);
      set_parameter(queue->getParameterId(), state.get(queue->getParameterId()), parameters);
    }
  }
}

static void set_parameter(Steinberg::Vst::ParamID parameter_id, double value, pf_parameters_t &parameters) {
  switch (parameter_id) {
    case kMasterVolume:
      pf_parameters_set_volume(value, &parameters);
      break;

    case kOsc1Waveform:
      pf_osc_parameters_set_waveform(value_to_osc_waveform(value), &parameters.osc1);
      break;

    case kOsc1Coarse:
      pf_osc_parameters_set_coarse(value, &parameters.osc1);
      break;

    case kOsc1FineTune:
      pf_osc_parameters_set_fine_tune(value, &parameters.osc1);
      break;

    case kOsc1PulseWidth:
      pf_osc_parameters_set_pulse_width(value, &parameters.osc1);
      break;

    case kOsc1PwmDepth:
      pf_osc_parameters_set_pwm_depth(value, &parameters.osc1);
      break;

    case kOsc1VibratoDepth:
      pf_osc_parameters_set_vibrato_depth(value, &parameters.osc1);
      break;

    case kOsc2Waveform:
      pf_osc_parameters_set_waveform(value_to_osc_waveform(value), &parameters.osc2);
      break;

    case kOsc2Coarse:
      pf_osc_parameters_set_coarse(value, &parameters.osc2);
      break;

    case kOsc2FineTune:
      pf_osc_parameters_set_fine_tune(value, &parameters.osc2);
      break;

    case kOsc2PulseWidth:
      pf_osc_parameters_set_pulse_width(value, &parameters.osc2);
      break;

    case kOsc2PwmDepth:
      pf_osc_parameters_set_pwm_depth(value, &parameters.osc2);
      break;

    case kOsc2VibratoDepth:
      pf_osc_parameters_set_vibrato_depth(value, &parameters.osc2);
      break;

    case kMixer1Ch1Volume:
      pf_mixer_ch_parameters_set_volume(value, &parameters.mixer.ch1);
      break;

    case kMixer1Ch1Enabled:
      pf_mixer_ch_parameters_set_enabled(value_to_mixer_ch_state(value), &parameters.mixer.ch1);
      break;

    case kMixer1Ch2Volume:
      pf_mixer_ch_parameters_set_volume(value, &parameters.mixer.ch2);
      break;

    case kMixer1Ch2Enabled:
      pf_mixer_ch_parameters_set_enabled(value_to_mixer_ch_state(value), &parameters.mixer.ch2);
      break;

    case kMixer1Ch3Volume:
      pf_mixer_ch_parameters_set_volume(value, &parameters.mixer.ch3);
      break;

    case kMixer1Ch3Enabled:
      pf_mixer_ch_parameters_set_enabled(value_to_mixer_ch_state(value), &parameters.mixer.ch3);
      break;

    case kFilter1Type:
      pf_filter_parameters_set_type(value_to_filter_type(value), &parameters.filter);
      break;

    case kFilter1KeyboardFollow:
      pf_filter_parameters_set_kbf_enabled(value_to_kbf_enabled(value), &parameters.filter);
      break;

    case kFilter1Cutoff:
      pf_filter_parameters_set_cutoff(value, &parameters.filter);
      break;

    case kFilter1Resonance:
      pf_filter_parameters_set_resonance(value, &parameters.filter);
      break;

    case kFilter1EnvDepth:
      pf_filter_parameters_set_env_depth(value, &parameters.filter);
      break;

    case kFilter1LfoDepth:
      pf_filter_parameters_set_lfo_depth(value, &parameters.filter);
      break;

    case kFilter1VelocityDepth:
      pf_filter_parameters_set_velocity_depth(value, &parameters.filter);
      break;

    case kFilter1ModWheelDepth:
      pf_filter_parameters_set_mod_wheel_depth(value, &parameters.filter);
      break;

    case kAdsr1Attack:
      adsr_set_attack(value, &parameters.adsr1);
      break;

    case kAdsr1Decay:
      adsr_set_decay(value, &parameters.adsr1);
      break;

    case kAdsr1Sustain:
      adsr_set_sustain(value, &parameters.adsr1);
      break;

    case kAdsr1Release:
      adsr_set_release(value, &parameters.adsr1);
      break;

    case kAdsr2Attack:
      adsr_set_attack(value, &parameters.adsr2);
      break;

    case kAdsr2Decay:
      adsr_set_decay(value, &parameters.adsr2);
      break;

    case kAdsr2Sustain:
      adsr_set_sustain(value, &parameters.adsr2);
      break;

    case kAdsr2Release:
      adsr_set_release(value, &parameters.adsr2);
      break;

    case kOscLfoWaveform:
      pf_lfo_parameters_set_waveform(value_to_lfo_waveform(value), &parameters.osc_lfo);
      break;

    case kOscLfoSpeed:
      pf_lfo_parameters_set_speed(value, &parameters.osc_lfo);
      pf_parameters_update_lfo_speed(parameters.tempo, &parameters.osc_lfo);
      break;

    case kOscLfoTempoSync:
      pf_lfo_parameters_set_tempo_sync(value_to_tempo_sync(value), &parameters.osc_lfo);
      pf_parameters_update_lfo_speed(parameters.tempo, &parameters.osc_lfo);
      break;

    case kFilterLfoWaveform:
      pf_lfo_parameters_set_waveform(value_to_lfo_waveform(value), &parameters.filter_lfo);
      break;

    case kFilterLfoSpeed:
      pf_lfo_parameters_set_speed(value, &parameters.filter_lfo);
      pf_parameters_update_lfo_speed(parameters.tempo, &parameters.filter_lfo);
      break;

    case kFilterLfoPhase:
      pf_lfo_parameters_set_phase(value, &parameters.filter_lfo);
      break;

    case kFilterLfoTempoSync:
      pf_lfo_parameters_set_tempo_sync(value_to_tempo_sync(value), &parameters.filter_lfo);
      pf_parameters_update_lfo_speed(parameters.tempo, &parameters.filter_lfo);
      break;

    case kFilterLfoRestart:
      pf_lfo_parameters_set_restart(value_to_lfo_restart(value), &parameters.filter_lfo);
      break;

    case kVelocityDepth:
      pf_parameters_set_velocity_depth(std::min(1.0, std::max(-1.0, value)), &parameters);
      break;

    case kVelocityCurve:
      pf_parameters_set_velocity_curve(std::min(1.0, std::max(-1.0, value)), &parameters);
      break;

    case kVoiceMode:
      pf_parameters_set_voice_mode(value_to_voice_mode(value), &parameters);
      break;

    case kPortamento:
      parameters.portamento.enabled = value_to_portamento_enabled(value);
      break;

    case kPortamentoTime:
      parameters.portamento.time = value;
      break;

    case kPitchBendRange:
      pf_parameters_set_pitch_bend_range(value, &parameters);
      break;

    case kPitchBend:
      pf_parameters_set_pitch_bend(value, &parameters);
      break;

    case kModWheel:
      pf_parameters_set_mod_wheel(value, &parameters);
      break;

    default:
      assert(false);
      break;
  }
}

void Processor::setDefaultParameterState() {
  m_state.set(kMasterVolume, -9.0);

  m_state.set(kOsc1Waveform, PF_OSC_WAVEFORM_PULSE);
  m_state.set(kOsc1PulseWidth, 0.55);
  m_state.set(kOsc1PwmDepth, 0.5);

  m_state.set(kFilter1Cutoff, 9600);
  m_state.set(kFilter1Resonance, 1.4);
  m_state.set(kFilter1EnvDepth, 1800);

  m_state.set(kAdsr1Decay, 800e-3);

  m_state.set(kOscLfoSpeed, 1.5);
}

static void set_state_parameters(const ParameterState& state,
                        pf_parameters_t &parameters,
                        polyfilur_t& polyfilur) {
  for (auto &parameter : state.parameters()) {
    set_parameter(parameter.first, parameter.second, parameters);
  }

  polyfilur_all_notes_off(&parameters, &polyfilur);
  polyfilur_update_parameters(&parameters, &polyfilur);
}

static void update_tempo(Steinberg::Vst::ProcessContext* process_context, pf_parameters_t &parameters) {
  if ((nullptr != process_context) &&
      (Steinberg::Vst::ProcessContext::kTempoValid & process_context->state)) {
    if (parameters.tempo != process_context->tempo) {
      pf_parameters_set_tempo(process_context->tempo, &parameters);
    }
  }
}

static void to_sample32(const double* left_buffer, const double* right_buffer, const Steinberg::Vst::ProcessData& data) {
  for (auto n = 0; n < data.numSamples; ++n) {
    data.outputs[0].channelBuffers32[0][n] = left_buffer[n];
    data.outputs[0].channelBuffers32[1][n] = right_buffer[n];
  }
}

static void to_sample64(const double* left_buffer, const double* right_buffer, const Steinberg::Vst::ProcessData& data) {
  memcpy(data.outputs[0].channelBuffers64[0], left_buffer, data.numSamples * sizeof(double));
  memcpy(data.outputs[0].channelBuffers64[1], right_buffer, data.numSamples * sizeof(double));
}

}  // namespace polyfilur

