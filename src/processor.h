/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of polyfilur.
 *
 * polyfilur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * polyfilur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with polyfilur.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef POLYFILUR_PROCESSOR_H
#define POLYFILUR_PROCESSOR_H

#include "parameter_state.h"

#include <filur/polyfilur/pf_parameters.h>
#include <filur/polyfilur/polyfilur.h>

#include <public.sdk/source/vst/vstaudioeffect.h>

namespace polyfilur {

class Processor : public Steinberg::Vst::AudioEffect {
 public:
  explicit Processor(const Steinberg::FUID& controller_id);

  Steinberg::tresult initialize(Steinberg::FUnknown* context) override;
  Steinberg::tresult setBusArrangements(Steinberg::Vst::SpeakerArrangement* inputs,
                                        Steinberg::int32 numIns,
                                        Steinberg::Vst::SpeakerArrangement* outputs,
                                        Steinberg::int32 numOuts) override;

  Steinberg::tresult setState(Steinberg::IBStream* state) override;
  Steinberg::tresult getState(Steinberg::IBStream* state) override;

  Steinberg::tresult canProcessSampleSize(Steinberg::int32 symbolicSampleSize) override;
  Steinberg::tresult setActive(Steinberg::TBool state) override;
  Steinberg::tresult process(Steinberg::Vst::ProcessData& data) override;

  static Steinberg::FUID cid;

 private:
  void setDefaultParameterState();

  ParameterState m_state;
  pf_parameters_t m_parameters = {};
  polyfilur_t m_polyfilur = {};

  std::vector<double> m_left_buffer;
  std::vector<double> m_right_buffer;

  Steinberg::TBool m_is_active = {0};
};

}  // namespace polyfilur

#endif // POLYFILUR_PROCESSOR_H
