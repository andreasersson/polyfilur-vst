/*
 * Copyright 2018-2019 Andreas Ersson
 *
 * This file is part of polyfilur.
 *
 * polyfilur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * polyfilur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with polyfilur.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "controller.h"
#include "version.h"

#include "parameter_ids.h"
#include "parameter_state.h"

#include <filurvst/common/parameter.h>
#include <filurvst/gui/aboutbox.h>
#include <filurvst/gui/uiviewcreator.h>

#include <pluginterfaces/base/ustring.h>
#include <pluginterfaces/vst/ivstmidicontrollers.h>

#include <vstgui/plugin-bindings/vst3editor.h>
#include <vstgui/uidescription/iviewfactory.h>
#include <vstgui/uidescription/uiattributes.h>

#include <initializer_list>
#include <string>

#ifdef USE_FONT_RESIZE
#include <filurvst/gui/editor.h>
using Editor = filurvst::gui::FontResizeEditor;
#else
using Editor = VSTGUI::VST3Editor;
#endif

namespace polyfilur {

using namespace filurvst;

Steinberg::FUID Controller::cid(0x51D49988, 0x5B144252, 0xB55CD185, 0xC39E39C0);

static VSTGUI::CView* custom_view_about_box(VSTGUI::UTF8StringPtr name,
                                            const VSTGUI::UIAttributes& attributes,
                                            const VSTGUI::IUIDescription* description,
                                            Steinberg::FObject* controller);
static VSTGUI::CView* custom_view_about_box_text(VSTGUI::UTF8StringPtr name,
                                                 const VSTGUI::UIAttributes& attributes,
                                                 const VSTGUI::IUIDescription* description);

Controller::Controller() :
    m_aboutbox(new gui::AboutBoxController, false) {
}

Steinberg::tresult  Controller::initialize(FUnknown* context) {
  Steinberg::tresult result = EditController::initialize(context);
  if (Steinberg::kResultTrue != result) {
    return result;
  }

  Steinberg::Vst::UnitID unit_id = Steinberg::Vst::kRootUnitId;

  ParameterState state;

  std::initializer_list<std::string> osc_waveform = { "sine", "saw", "pulse" };
  std::initializer_list<std::string> filter_type = { "lowpass", "highpass", "bandpass" };
  std::initializer_list<std::string> mixer_enabled = { "off", "on", "bypass" };
  std::initializer_list<std::string> lfo_waveform = { "sine", "triangle", "saw" };
  std::initializer_list<std::string> tempo_sync = { "off", "16th", "8th", "4th", "2th", "1", "2", "4" };
  std::initializer_list<std::string> voice_mode = { "poly", "mono", "legato" };

  add_range_parameter(parameters, state, "volume", kMasterVolume, unit_id);
  add_parameter(parameters, state, "velcoity depth", kVelocityDepth, unit_id);
  add_range_parameter(parameters, state, "curve", kVelocityCurve, unit_id);

  // oscillator 1
  add_list_parameter(parameters, state, "oscillator 1 waveform", kOsc1Waveform, osc_waveform, unit_id);
  {
    double min = state.min(kOsc1Coarse) / 100;
    double max = state.max(kOsc1Coarse) / 100;
    double step_count = max - min;
    add_range_parameter(parameters, state, "oscillator 1 coarse", kOsc1Coarse, min, max, unit_id, step_count);
  }
  add_range_parameter_step(parameters, state, "oscillator 1 fine tune", kOsc1FineTune, unit_id);
  add_parameter(parameters, state, "oscillator 1 pulse width", kOsc1PulseWidth, unit_id);
  add_parameter(parameters, state, "oscillator 1 pwm depth", kOsc1PwmDepth, unit_id);
  add_parameter(parameters, state, "oscillator 1 vibrato depth", kOsc1VibratoDepth, unit_id);

  // oscillator 2
  add_list_parameter(parameters, state, "oscillator 2 waveform", kOsc2Waveform, osc_waveform, unit_id);
  {
    double min = state.min(kOsc2Coarse) / 100;
    double max = state.max(kOsc2Coarse) / 100;
    double step_count = max - min;
    add_range_parameter(parameters, state, "oscillator 2 coarse", kOsc2Coarse, min, max, unit_id, step_count);
  }
  add_range_parameter_step(parameters, state, "oscillator 2 fine tune", kOsc2FineTune, unit_id);
  add_parameter(parameters, state, "oscillator 2 pulse width", kOsc2PulseWidth, unit_id);
  add_parameter(parameters, state, "oscillator 2 pwm depth", kOsc2PwmDepth, unit_id);
  add_parameter(parameters, state, "oscillator 2 vibrato depth", kOsc2VibratoDepth, unit_id);

  // filter
  add_list_parameter(parameters, state, "filter type", kFilter1Type, filter_type, unit_id);
  add_range_parameter(parameters, state, "filter keyboard follow", kFilter1KeyboardFollow, 0, 1, unit_id, 1);
  add_parameter(parameters, state, "filter cutoff", kFilter1Cutoff, unit_id);
  add_parameter(parameters, state, "filter resonance", kFilter1Resonance, unit_id);
  add_range_parameter(parameters, state, "filter envelope depth", kFilter1EnvDepth, -1.0, 1.0, unit_id);
  add_range_parameter(parameters, state, "filter lfo depth", kFilter1LfoDepth, -1.0, 1.0, unit_id);
  add_range_parameter(parameters, state, "filter velocity depth", kFilter1VelocityDepth, -1.0, 1.0, unit_id);
  add_range_parameter(parameters, state, "filter modwheel depth", kFilter1ModWheelDepth, -1.0, 1.0, unit_id);

  // mixer
  add_range_parameter(parameters, state, "oscillator 1 volume", kMixer1Ch1Volume, unit_id);
  add_list_parameter(parameters, state, "oscillator 1 enabled", kMixer1Ch1Enabled, mixer_enabled, unit_id);
  add_range_parameter(parameters, state, "oscillator 2 volume", kMixer1Ch2Volume, unit_id);
  add_list_parameter(parameters, state, "oscillator 2 enabled", kMixer1Ch2Enabled, mixer_enabled, unit_id);
  add_range_parameter(parameters, state, "noise volume", kMixer1Ch3Volume, unit_id);
  add_list_parameter(parameters, state, "noise enabled", kMixer1Ch3Enabled, mixer_enabled, unit_id);

  // envelope 1
  add_parameter(parameters, state, "adsr 1 attack", kAdsr1Attack, unit_id);
  add_parameter(parameters, state, "adsr 1 decay", kAdsr1Decay, unit_id);
  add_parameter(parameters, state, "adsr 1 sustain", kAdsr1Sustain, unit_id);
  add_parameter(parameters, state, "adsr 1 release", kAdsr1Release, unit_id);

  // envelope 2
  add_parameter(parameters, state, "adsr 2 attack", kAdsr2Attack, unit_id);
  add_parameter(parameters, state, "adsr 2 decay", kAdsr2Decay, unit_id);
  add_parameter(parameters, state, "adsr 2 sustain", kAdsr2Sustain, unit_id);
  add_parameter(parameters, state, "adsr 2 release", kAdsr2Release, unit_id);

  // oscillator lfo
  add_list_parameter(parameters, state, "Oscillator LFO Waveform", kOscLfoWaveform, lfo_waveform, unit_id);
  add_parameter(parameters, state, "oscillator lfo speed", kOscLfoSpeed, unit_id);
  add_list_parameter(parameters, state, "oscillator lfo tempo sync", kOscLfoTempoSync, tempo_sync, unit_id);

  // filter lfo
  add_list_parameter(parameters, state, "filter lfo waveform", kFilterLfoWaveform, lfo_waveform, unit_id);
  add_parameter(parameters, state, "filter lfo speed", kFilterLfoSpeed, unit_id);
  add_parameter(parameters, state, "filter lfo phase", kFilterLfoPhase, unit_id);
  add_list_parameter(parameters, state, "filter lfo tempo sync", kFilterLfoTempoSync, tempo_sync, unit_id);
  add_range_parameter(parameters, state, "filter lfo restart", kFilterLfoRestart, 0, 1, unit_id, 1);

  add_list_parameter(parameters, state, "voice mode", kVoiceMode, voice_mode, unit_id);

  add_range_parameter(parameters, state, "portamento", kPortamento, 0, 1, unit_id, 1);
  add_parameter(parameters, state, "portamento time", kPortamentoTime, unit_id);

  add_range_parameter_step(parameters, state, "pitch bend range", kPitchBendRange, unit_id);
  add_parameter(parameters, state, "pitch bend", kPitchBend, unit_id);
  add_parameter(parameters, state, "modwheel", kModWheel, unit_id);

  return Steinberg::kResultTrue;
}

Steinberg::tresult  Controller::terminate() {
  return EditController::terminate();
}

Steinberg::tresult  Controller::setComponentState(Steinberg::IBStream* state_stream) {
  ParameterState state;

  Steinberg::tresult result = state.setState(state_stream);

  for (auto &parameter : state.parameters()) {
    setParamNormalized(parameter.first, state.getNormalized(parameter.first));
  }

  return result;
}

Steinberg::tresult  Controller::getMidiControllerAssignment(Steinberg::int32 bus_index,
                                                            Steinberg::int16 channel,
                                                            Steinberg::Vst::CtrlNumber midi_cc,
                                                            Steinberg::Vst::ParamID& id) {
  Steinberg::tresult ret_val = Steinberg::kResultFalse;
  Steinberg::Vst::ParamID local_id = Steinberg::Vst::kNoParamId;

  if ((bus_index == 0) && (channel == 0)) {
    switch (midi_cc) {
      case Steinberg::Vst::kPitchBend:
        local_id = kPitchBend;
        break;

      case Steinberg::Vst::kCtrlModWheel:
        local_id = kModWheel;
        break;
    }
  }

  if (Steinberg::Vst::kNoParamId != local_id) {
    ret_val = Steinberg::kResultTrue;
    id = local_id;
  }

  return ret_val;
}

Steinberg::IPlugView* Controller::createView(Steinberg::FIDString name) {
  Steinberg::ConstString local_name(name);
  Steinberg::IPlugView *plug_view = nullptr;

  gui::ui_view_creator_init();
  if (Steinberg::Vst::ViewType::kEditor == local_name) {
    plug_view = new Editor(this, "Editor", "polyfilur.uidesc");
  }

  return plug_view;
}

Steinberg::tresult Controller::openAboutBox(Steinberg::TBool only_check) {
  Steinberg::tresult ret_val = Steinberg::kResultTrue;
  if (!only_check && m_aboutbox) {
    m_aboutbox->open();
  }

  return ret_val;
}

VSTGUI::CView* Controller::createCustomView(VSTGUI::UTF8StringPtr name,
                                            const VSTGUI::UIAttributes& attributes,
                                            const VSTGUI::IUIDescription* description,
                                            VSTGUI::VST3Editor* /*editor*/) {
  assert(nullptr != description);

  VSTGUI::CView* view = custom_view_about_box(name, attributes, description, m_aboutbox.get());

  if (nullptr == view) {
    view = custom_view_about_box_text(name, attributes, description);
  }

  return view;
}

VSTGUI::COptionMenu* Controller::createContextMenu(const VSTGUI::CPoint& /*pos*/, VSTGUI::VST3Editor* /*editor*/) {
  VSTGUI::COptionMenu* menu = nullptr;

  if (m_aboutbox) {
    std::unique_ptr<VSTGUI::COptionMenu> option_menu(new VSTGUI::COptionMenu());
    std::unique_ptr<VSTGUI::CCommandMenuItem> item(new VSTGUI::CCommandMenuItem(VSTGUI::CCommandMenuItem::Desc("About")));
    item->setActions([aboutbox = m_aboutbox.get()](VSTGUI::CCommandMenuItem*){ aboutbox->open(); });
    option_menu->addEntry(item.release());

    menu = option_menu.release();
  }

  return menu;
}

VSTGUI::CView* custom_view_about_box(VSTGUI::UTF8StringPtr name,
                                     const VSTGUI::UIAttributes& attributes,
                                     const VSTGUI::IUIDescription* description,
                                     Steinberg::FObject* controller) {
  VSTGUI::CView* view = nullptr;
  const VSTGUI::IViewFactory* factory = description->getViewFactory();
  if ((nullptr != factory) &&
      (nullptr != name) &&
      (strcmp(name, "AboutBox") == 0)) {
    view = factory->createView(attributes, description);
    gui::AboutBox* aboutbox = dynamic_cast<gui::AboutBox*>(view);
    if (nullptr != aboutbox) {
      aboutbox->setController(controller);
    }
  }

  return view;
}

VSTGUI::CView* custom_view_about_box_text(VSTGUI::UTF8StringPtr name,
                                     const VSTGUI::UIAttributes& attributes,
                                     const VSTGUI::IUIDescription* description) {
  VSTGUI::CView* view = nullptr;
  const VSTGUI::IViewFactory* factory = description->getViewFactory();
  if ((nullptr != factory) &&
      (nullptr != name) &&
      (strcmp(name, "AboutBoxText") == 0)) {
    std::string about_text;
    about_text += stringPluginName "\n";
    about_text += "version: " FULL_VERSION_STR "\n";
    about_text += stringCompanyWeb"\n\n";
    about_text += stringLegalCopyright"\n";

    about_text += "This program comes with ABSOLUTELY NO WARRANTY.\n";
    about_text += "This is free software, and you are welcome to redistribute it under certain conditions.\n";
    about_text += "You should have received a copy of the GNU General Public License along with " stringPluginName ".\n";
    about_text += "If not, see <https://www.gnu.org/licenses/>.\n";

    VSTGUI::UIAttributes local_attributes = attributes;
    local_attributes.setAttribute("title", about_text);
    view = factory->createView(local_attributes, description);
  }

  return view;
}

}  // namespace polyfilur
