/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of polyfilur.
 *
 * polyfilur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * polyfilur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with polyfilur.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef POLYFILUR_PARAMETER_STATE_H
#define POLYFILUR_PARAMETER_STATE_H

#include <filurvst/common/parameter_state.h>

namespace polyfilur {

class ParameterState : public filurvst::State {
 public:
  ParameterState();
  virtual ~ParameterState() {}

  double keboard_follow_factor;
};

}  // namespace polyfilur

#endif // POLYFILUR_PARAMETER_STATE_H
