# Virtual analog synthesizer VST3 plug-in

<img src="https://gitlab.com/andreasersson/polyfilur-vst/wikis/uploads/84bd748eafa7cc7471ddf58ec9413f61/polyfilur.png" alt="polyfilur screenshot" width="680"/>
<img src="https://gitlab.com/andreasersson/polyfilur-vst/wikis/uploads/d66ee472c5d11504aaf56aecd8d54fe9/VST_Compatible_Logo_Steinberg_with_TM_negative.svg" alt="VST Compatible Logo" width="150"/>

---
## How to build.
### Requirements
- [CMake][CMake] 3.11.0 or later.

### Dependencies
- [Steinberg VST 3 Plug-In SDK][VST3SDK]
- [googletest][googletest]. Only used if BUILD_TESTS is enabled.
- [filur][filur]
- [filur-vst][filur-vst]

*The dependencies will be automatically cloned and built using the [CMake FetchContent module][FetchContent].*  
*On some Linux distributions you might have to install the dependencies needed by [VSTGUI][VSTGUI].*

Download the source or clone the git repository.

    git clone https://gitlab.com/andreasersson/polyfilur-vst.git

### Linux/macOS with Makefiles
*Note that Linux support is still experimental in [VST3SDK][VST3SDK].*

    cmake -DCMAKE_BUILD_TYPE=Release -S polyfilur-vst -B build-polyfilur
    cmake --build build-polyfilur

### macOS with Xcode
    cmake -GXcode -S polyfilur-vst -B build-polyfilur
    cmake --build build-polyfilur --config Release

### Windows with Visual Studio 16 2019
    cmake -G"Visual Studio 16 2019" -S polyfilur-vst -B build-polyfilur
    cmake --build build-polyfilur --config Release

## Installation
Copy the VST3 bundle, [build folder]/VST3/Release/polyfilur.vst3, to one of the specific VST3 folders.
#### macOS
- /Users/$USERNAME/Library/Audio/Plug-ins/VST3/
- /Library/Audio/Plug-ins/VST3/
- $APPFOLDER/Contents/VST3/

#### Linux
- $HOME/.vst3/
- /usr/lib/vst3/
- /usr/local/lib/vst3/
- $APPFOLDER/vst3/

#### Windows
- /Program Files/Common Files/VST3/
- $APPFOLDER/VST3/

## License
[![GPLv3](https://www.gnu.org/graphics/gplv3-with-text-136x68.png "GNU General Public License")](https://www.gnu.org/licenses/gpl.html)

    polyfilur is free software: you can redistribute it and/or modify   
    it under the terms of the GNU General Public License as published by   
    the Free Software Foundation, either version 3 of the License, or   
    (at your option) any later version.   

    polyfilur is distributed in the hope that it will be useful,   
    but WITHOUT ANY WARRANTY; without even the implied warranty of   
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the   
    GNU General Public License for more details.

[CMake]: https://cmake.org/
[FetchContent]: https://cmake.org/cmake/help/latest/module/FetchContent.html
[googletest]: https://github.com/abseil/googletest
[VST3SDK]: https://github.com/steinbergmedia/vst3sdk
[VSTGUI]: https://github.com/steinbergmedia/vstgui
[filur]: https://gitlab.com/andreasersson/filur
[filur-vst]: https://gitlab.com/andreasersson/filur-vst
