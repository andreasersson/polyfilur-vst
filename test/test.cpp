/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of polyfilur.
 *
 * polyfilur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * polyfilur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with polyfilur.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <gtest/gtest.h>

#include "../src/parameter_state.cpp"
#include "../src/processor.cpp"

#include <filur/tempo_sync.h>

#include <public.sdk/source/common/memorystream.h>

#include <random>
#include <type_traits>

#define STATE_EXPECT_EQ(state1, state2) \
  for (auto& parameter : state1.parameters()) {\
    filurvst::parameter_info_t info;\
    ASSERT_TRUE(state1.info(parameter.first, info));\
    if (!info.runtime) {\
      EXPECT_EQ(state1.get(parameter.first), state2.get(parameter.first));\
    }\
  }

template <class T> T pf_cast(double value) {
  return static_cast<T>(value);
}

template <> pf_osc_waveform_t pf_cast(double value) {
  return polyfilur::value_to_osc_waveform(value);
}

template <> pf_filter_type_t pf_cast(double value) {
  return polyfilur::value_to_filter_type(value);
}

template <> pf_kbf_enabled_t pf_cast(double value) {
  return polyfilur::value_to_kbf_enabled(value);
}

template <> pf_mixer_ch_state_t pf_cast(double value) {
  return polyfilur::value_to_mixer_ch_state(value);
}

template <> pf_lfo_waveform_t pf_cast(double value) {
  return polyfilur::value_to_lfo_waveform(value);
}

template <> pf_lfo_restart_t pf_cast(double value) {
  return polyfilur::value_to_lfo_restart(value);
}

template <> pf_portamento_enabled_t pf_cast(double value) {
  return polyfilur::value_to_portamento_enabled(value);
}

template <> pf_voice_mode_t pf_cast(double value) {
  return polyfilur::value_to_voice_mode(value);
}

template <> tempo_sync_t pf_cast(double value) {
  return value_to_tempo_sync(value);
}

static void test_setup(filurvst::State& state1,
                       filurvst::State& state2,
                       std::uniform_real_distribution<double> distribution) {
  std::default_random_engine generator;
  Steinberg::MemoryStream stream;
  for (auto& parameter : state1.parameters()) {
    state1.setNormalized(parameter.first, distribution(generator));
  }

  state1.getState(&stream);
  stream.seek(0, Steinberg::IBStream::kIBSeekSet, nullptr);
  state2.setState(&stream);
}

static void test_setup_min(filurvst::State& state1, filurvst::State& state2) {
  std::uniform_real_distribution<double> distribution(0.0, 0.0);
  test_setup(state1, state2, distribution);
}

static void test_setup_max(filurvst::State& state1, filurvst::State& state2) {
  std::uniform_real_distribution<double> distribution(1.0, 1.0);
  test_setup(state1, state2, distribution);
}

static void test_setup_rnd(filurvst::State& state1, filurvst::State& state2) {
  std::uniform_real_distribution<double> distribution(0.0, 1.0);
  test_setup(state1, state2, distribution);
}

template<class T> bool test_set_parameter(polyfilur::ParameterState& state,
                                          Steinberg::Vst::ParamID parameter_id,
                                          pf_parameters_t& parameters,
                                          T& value) {
  state.setNormalized(parameter_id, 0.0);
  T min_value = pf_cast<T>(state.get(parameter_id));
  polyfilur::set_parameter(parameter_id, min_value, parameters);
  T pre_value = value;

  state.setNormalized(parameter_id, 1.0);
  T max_value = pf_cast<T>(state.get(parameter_id));
  polyfilur::set_parameter(parameter_id, max_value, parameters);

  bool ret_val = (pre_value != value);

  return ret_val;
}

TEST(ParameterTo, OscWaveform) {
  EXPECT_EQ(PF_OSC_WAVEFORM_SINE, polyfilur::value_to_osc_waveform(PF_OSC_WAVEFORM_SINE));
  EXPECT_EQ(PF_OSC_WAVEFORM_SAW, polyfilur::value_to_osc_waveform(PF_OSC_WAVEFORM_SAW));
  EXPECT_EQ(PF_OSC_WAVEFORM_PULSE, polyfilur::value_to_osc_waveform(PF_OSC_WAVEFORM_PULSE));
}

TEST(ParameterTo, FilterType) {
  EXPECT_EQ(PF_FILTER_TYPE_LOWPASS, polyfilur::value_to_filter_type(PF_FILTER_TYPE_LOWPASS));
  EXPECT_EQ(PF_FILTER_TYPE_HIGHPASS, polyfilur::value_to_filter_type(PF_FILTER_TYPE_HIGHPASS));
  EXPECT_EQ(PF_FILTER_TYPE_BANDPASS, polyfilur::value_to_filter_type(PF_FILTER_TYPE_BANDPASS));
}

TEST(ParameterTo, KbfEnabled) {
  EXPECT_EQ(PF_KBF_DISABLED, polyfilur::value_to_kbf_enabled(PF_KBF_DISABLED));
  EXPECT_EQ(PF_KBF_ENABLED, polyfilur::value_to_kbf_enabled(PF_KBF_ENABLED));
}

TEST(ParameterTo, MixerChState) {
  EXPECT_EQ(PF_MIXER_CH_OFF, polyfilur::value_to_mixer_ch_state(PF_MIXER_CH_OFF));
  EXPECT_EQ(PF_MIXER_CH_ON, polyfilur::value_to_mixer_ch_state(PF_MIXER_CH_ON));
  EXPECT_EQ(PF_MIXER_CH_BYPASS, polyfilur::value_to_mixer_ch_state(PF_MIXER_CH_BYPASS));
}

TEST(ParameterTo, LfoWaveform) {
  EXPECT_EQ(PF_LFO_WAVEFORM_SINE, polyfilur::value_to_lfo_waveform(PF_LFO_WAVEFORM_SINE));
  EXPECT_EQ(PF_LFO_WAVEFORM_TRIANGLE, polyfilur::value_to_lfo_waveform(PF_LFO_WAVEFORM_TRIANGLE));
  EXPECT_EQ(PF_LFO_WAVEFORM_SAW, polyfilur::value_to_lfo_waveform(PF_LFO_WAVEFORM_SAW));
}

TEST(ParameterTo, LfoRestart) {
  EXPECT_EQ(PF_LFO_RESTART_FALSE, polyfilur::value_to_lfo_restart(PF_LFO_RESTART_FALSE));
  EXPECT_EQ(PF_LFO_RESTART_TRUE, polyfilur::value_to_lfo_restart(PF_LFO_RESTART_TRUE));
}

TEST(ParameterTo, PortamentoEnabled) {
  EXPECT_EQ(PF_PORTAMENTO_DISABLED, polyfilur::value_to_portamento_enabled(PF_PORTAMENTO_DISABLED));
  EXPECT_EQ(PF_PORTAMENTO_ENABLED, polyfilur::value_to_portamento_enabled(PF_PORTAMENTO_ENABLED));
}

TEST(ParameterTo, VoiceMode) {
  EXPECT_EQ(PF_VOICE_MODE_POLY, polyfilur::value_to_voice_mode(PF_VOICE_MODE_POLY));
  EXPECT_EQ(PF_VOICE_MODE_MONO, polyfilur::value_to_voice_mode(PF_VOICE_MODE_MONO));
  EXPECT_EQ(PF_VOICE_MODE_MONOLEGATO, polyfilur::value_to_voice_mode(PF_VOICE_MODE_MONOLEGATO));
}

TEST(ParameterState, GetSetState) {
  polyfilur::ParameterState state1;
  polyfilur::ParameterState state2;

  test_setup_min(state1, state2);
  STATE_EXPECT_EQ(state1, state2)

  test_setup_max(state1, state2);
  STATE_EXPECT_EQ(state1, state2)

  test_setup_rnd(state1, state2);
  STATE_EXPECT_EQ(state1, state2)
}

TEST(Parameter, setParameter) {
  polyfilur::ParameterState state;
  pf_parameters_t parameters;
  pf_parameters_init(48000.0, state.keboard_follow_factor, &parameters);

  EXPECT_TRUE(test_set_parameter(state, polyfilur::kOsc1Waveform, parameters, parameters.osc1.waveform));
  EXPECT_TRUE(test_set_parameter(state, polyfilur::kOsc1PulseWidth, parameters, parameters.osc1.pulse_width));
  EXPECT_TRUE(test_set_parameter(state, polyfilur::kOsc1PwmDepth, parameters, parameters.osc1.pwm_depth));
  EXPECT_TRUE(test_set_parameter(state, polyfilur::kOsc1Coarse, parameters, parameters.osc1.coarse));
  EXPECT_TRUE(test_set_parameter(state, polyfilur::kOsc1FineTune, parameters, parameters.osc1.fine_tune));
  EXPECT_TRUE(test_set_parameter(state, polyfilur::kOsc1VibratoDepth, parameters, parameters.osc1.vibrato_depth));

  EXPECT_TRUE(test_set_parameter(state, polyfilur::kOsc2Waveform, parameters, parameters.osc2.waveform));
  EXPECT_TRUE(test_set_parameter(state, polyfilur::kOsc2PulseWidth, parameters, parameters.osc2.pulse_width));
  EXPECT_TRUE(test_set_parameter(state, polyfilur::kOsc2PwmDepth, parameters, parameters.osc2.pwm_depth));
  EXPECT_TRUE(test_set_parameter(state, polyfilur::kOsc2Coarse, parameters, parameters.osc2.coarse));
  EXPECT_TRUE(test_set_parameter(state, polyfilur::kOsc2FineTune, parameters, parameters.osc2.fine_tune));
  EXPECT_TRUE(test_set_parameter(state, polyfilur::kOsc2VibratoDepth, parameters, parameters.osc2.vibrato_depth));

  EXPECT_TRUE(test_set_parameter(state, polyfilur::kFilter1Type, parameters, parameters.filter.type));
  EXPECT_TRUE(test_set_parameter(state, polyfilur::kFilter1KeyboardFollow, parameters, parameters.filter.keyboard_follow_enabled));
  EXPECT_TRUE(test_set_parameter(state, polyfilur::kFilter1Cutoff, parameters, parameters.filter.cutoff));
  EXPECT_TRUE(test_set_parameter(state, polyfilur::kFilter1Resonance, parameters, parameters.filter.resonance));
  EXPECT_TRUE(test_set_parameter(state, polyfilur::kFilter1EnvDepth, parameters, parameters.filter.env_depth));
  EXPECT_TRUE(test_set_parameter(state, polyfilur::kFilter1LfoDepth, parameters, parameters.filter.lfo_depth));
  EXPECT_TRUE(test_set_parameter(state, polyfilur::kFilter1VelocityDepth, parameters, parameters.filter.velocity_depth));
  EXPECT_TRUE(test_set_parameter(state, polyfilur::kFilter1ModWheelDepth, parameters, parameters.filter.mod_wheel_depth));

  EXPECT_TRUE(test_set_parameter(state, polyfilur::kMixer1Ch1Volume, parameters, parameters.mixer.ch1.volume));
  EXPECT_TRUE(test_set_parameter(state, polyfilur::kMixer1Ch1Enabled, parameters, parameters.mixer.ch1.enabled));
  EXPECT_TRUE(test_set_parameter(state, polyfilur::kMixer1Ch2Volume, parameters, parameters.mixer.ch2.volume));
  EXPECT_TRUE(test_set_parameter(state, polyfilur::kMixer1Ch2Enabled, parameters, parameters.mixer.ch2.enabled));
  EXPECT_TRUE(test_set_parameter(state, polyfilur::kMixer1Ch3Volume, parameters, parameters.mixer.ch3.volume));
  EXPECT_TRUE(test_set_parameter(state, polyfilur::kMixer1Ch3Enabled, parameters, parameters.mixer.ch3.enabled));

  EXPECT_TRUE(test_set_parameter(state, polyfilur::kAdsr1Attack, parameters, parameters.adsr1.attack));
  EXPECT_TRUE(test_set_parameter(state, polyfilur::kAdsr1Decay, parameters, parameters.adsr1.decay));
  EXPECT_TRUE(test_set_parameter(state, polyfilur::kAdsr1Sustain, parameters, parameters.adsr1.sustain));
  EXPECT_TRUE(test_set_parameter(state, polyfilur::kAdsr1Release, parameters, parameters.adsr1.release));

  EXPECT_TRUE(test_set_parameter(state, polyfilur::kAdsr2Attack, parameters, parameters.adsr2.attack));
  EXPECT_TRUE(test_set_parameter(state, polyfilur::kAdsr2Decay, parameters, parameters.adsr2.decay));
  EXPECT_TRUE(test_set_parameter(state, polyfilur::kAdsr2Sustain, parameters, parameters.adsr2.sustain));
  EXPECT_TRUE(test_set_parameter(state, polyfilur::kAdsr2Release, parameters, parameters.adsr2.release));

  EXPECT_TRUE(test_set_parameter(state, polyfilur::kOscLfoWaveform, parameters, parameters.osc_lfo.waveform));
  EXPECT_TRUE(test_set_parameter(state, polyfilur::kOscLfoSpeed, parameters, parameters.osc_lfo.speed));
  EXPECT_TRUE(test_set_parameter(state, polyfilur::kOscLfoTempoSync, parameters, parameters.osc_lfo.tempo_sync));

  EXPECT_TRUE(test_set_parameter(state, polyfilur::kFilterLfoWaveform, parameters, parameters.filter_lfo.waveform));
  EXPECT_TRUE(test_set_parameter(state, polyfilur::kFilterLfoSpeed, parameters, parameters.filter_lfo.speed));
  EXPECT_TRUE(test_set_parameter(state, polyfilur::kFilterLfoTempoSync, parameters, parameters.filter_lfo.tempo_sync));
  EXPECT_TRUE(test_set_parameter(state, polyfilur::kFilterLfoRestart, parameters, parameters.filter_lfo.restart));
  EXPECT_TRUE(test_set_parameter(state, polyfilur::kFilterLfoPhase, parameters, parameters.filter_lfo.phase));

  EXPECT_TRUE(test_set_parameter(state, polyfilur::kPortamento, parameters, parameters.portamento.enabled));
  EXPECT_TRUE(test_set_parameter(state, polyfilur::kPortamentoTime, parameters, parameters.portamento.time));

  EXPECT_TRUE(test_set_parameter(state, polyfilur::kMasterVolume, parameters, parameters.volume));
  EXPECT_TRUE(test_set_parameter(state, polyfilur::kVelocityDepth, parameters, parameters.velocity_depth));
  EXPECT_TRUE(test_set_parameter(state, polyfilur::kVelocityCurve, parameters, parameters.velocity_curve));
  EXPECT_TRUE(test_set_parameter(state, polyfilur::kVoiceMode, parameters, parameters.voice_mode));
  EXPECT_TRUE(test_set_parameter(state, polyfilur::kPitchBendRange, parameters, parameters.pitch_bend_range));
  EXPECT_TRUE(test_set_parameter(state, polyfilur::kPitchBend, parameters, parameters.pitch_bend));
  EXPECT_TRUE(test_set_parameter(state, polyfilur::kModWheel, parameters, parameters.mod_wheel));
}

TEST(UpdateTempo, NullPointer) {
  pf_parameters_t parameters;
  pf_parameters_init(48000.0, 0, &parameters);
  parameters.tempo = 90.0;

  polyfilur::update_tempo(nullptr, parameters);
  EXPECT_EQ(90.0, parameters.tempo);
}

TEST(UpdateTempo, NotTempoValid) {
  pf_parameters_t parameters;
  pf_parameters_init(48000.0, 0, &parameters);
  parameters.tempo = 0.0;
  Steinberg::Vst::ProcessContext process_context;
  process_context.tempo = 120.0;
  process_context.state = 0;

  polyfilur::update_tempo(&process_context, parameters);
  EXPECT_NE(process_context.tempo, parameters.tempo);

}

TEST(UpdateTempo, TempoValid) {
  pf_parameters_t parameters;
  pf_parameters_init(48000.0, 0, &parameters);
  parameters.tempo = 0.0;
  Steinberg::Vst::ProcessContext process_context;
  process_context.tempo = 120.0;
  process_context.state = Steinberg::Vst::ProcessContext::kTempoValid;

  polyfilur::update_tempo(&process_context, parameters);
  EXPECT_EQ(process_context.tempo, parameters.tempo);
}

TEST(Process, to_sample32) {
  constexpr size_t num_samples = 512;
  double left_buffer[num_samples];
  double right_buffer[num_samples];

  float out_left[num_samples];
  float out_right[num_samples];
  float* buffers[] = {out_left, out_right};
  Steinberg::Vst::AudioBusBuffers audio_bus_buffers;
  audio_bus_buffers.channelBuffers32 = buffers;

  Steinberg::Vst::ProcessData process_data;
  process_data.outputs = &audio_bus_buffers;
  process_data.numSamples = num_samples / 2;

  memset(out_left, 0, sizeof(out_left));
  memset(out_right, 0, sizeof(out_left));

  for (size_t n = 0; n < num_samples; ++n) {
    left_buffer[n] = static_cast<double>(n) / num_samples;
    right_buffer[n] = -static_cast<double>(n) / num_samples;
  }

  polyfilur::to_sample32(left_buffer, right_buffer, process_data);

  for (int n = 0; n < process_data.numSamples; ++n) {
    EXPECT_EQ(out_left[n], static_cast<float>(left_buffer[n]));
    EXPECT_EQ(out_right[n], static_cast<float>(right_buffer[n]));
  }

  EXPECT_EQ(out_left[process_data.numSamples], 0.0);
  EXPECT_EQ(out_right[process_data.numSamples], 0.0);
}

TEST(Process, to_sample64) {
  constexpr size_t num_samples = 512;
  double left_buffer[num_samples];
  double right_buffer[num_samples];

  double out_left[num_samples];
  double out_right[num_samples];
  double* buffers[] = {out_left, out_right};
  Steinberg::Vst::AudioBusBuffers audio_bus_buffers;
  audio_bus_buffers.channelBuffers64 = buffers;

  Steinberg::Vst::ProcessData process_data;
  process_data.outputs = &audio_bus_buffers;
  process_data.numSamples = num_samples / 2;

  memset(out_left, 0, sizeof(out_left));
  memset(out_right, 0, sizeof(out_left));

  for (size_t n = 0; n < num_samples; ++n) {
    left_buffer[n] = static_cast<double>(n) / num_samples;
    right_buffer[n] = -static_cast<double>(n) / num_samples;
  }

  polyfilur::to_sample64(left_buffer, right_buffer, process_data);

  for (int n = 0; n < process_data.numSamples; ++n) {
    EXPECT_EQ(out_left[n], left_buffer[n]);
    EXPECT_EQ(out_right[n], right_buffer[n]);
  }

  EXPECT_EQ(out_left[process_data.numSamples], 0.0);
  EXPECT_EQ(out_right[process_data.numSamples], 0.0);
}
